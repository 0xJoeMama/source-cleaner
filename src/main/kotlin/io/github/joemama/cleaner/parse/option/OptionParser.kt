package io.github.joemama.cleaner.parse.option

import java.util.*

class OptionParser(private val args: Array<String>) {
    private val options: List<Option<*>> by lazy { this.initializeOptions(args) }

    private fun initializeOptions(args: Array<String>): List<Option<*>> {
        val options = mutableListOf<Option<*>>()
        val optionTypes = enumValues<OptionType>()
        optionTypes.forEach { type ->
            val optionIndex = args.indexOf(type.trigger)

            if (optionIndex >= 0) {
                assert(optionIndex + type.argCount < args.size) {
                    "Invalid arguments for trigger ${args[optionIndex]}"
                }
                this.initializeOption(type, optionIndex) { options.add(it) }
            }
        }

        return options
    }

    private fun initializeOption(type: OptionType, optionIndex: Int, adder: (Option<*>) -> Unit) {
        when (type.argCount) {
            0 -> this.initializeBooleanOption(type, adder)
            else -> {
                try {
                    this.parseAsInt(type, optionIndex, adder)
                } catch (e: NumberFormatException) {
                    this.initializeStringOption(type, optionIndex, adder)
                }
            }
        }
    }

    private fun parseAsInt(type: OptionType, optionIndex: Int, accumulator: (Option<*>) -> Unit) {
        accumulator(Option(type, this.args[optionIndex + 1].toInt()))
    }

    private fun initializeStringOption(type: OptionType, optionIndex: Int, accumulator: (Option<*>) -> Unit) {
        assert(args[optionIndex + 1].isNotEmpty()) { "Issue occured during option indexing" }

        accumulator(Option(type, args[optionIndex + 1]))
    }

    private fun initializeBooleanOption(type: OptionType, accumulator: (Option<*>) -> Unit) {
        accumulator(Option(type, true))
    }

    operator fun get(type: OptionType): Option<*> {
        return try {
            this.options.first { it.optionType == type }
        } catch (e: NoSuchElementException) {
            type.default()
        }
    }
}

enum class OptionType(val trigger: String, val argCount: Int = 0) {
    TARGET_DIR("-d", 1),
    RECURSIVE("-r"),
    SAFE_MODE("-s"),
    CONFIG("-c", 1),
    HELP("-h");

    companion object {
        private val defaults: EnumMap<OptionType, Option<*>> by lazy {
            val map = EnumMap<OptionType, Option<*>>(OptionType::class.java)
            map[TARGET_DIR] = Option(TARGET_DIR, "./")
            map[RECURSIVE] = Option(RECURSIVE, false)
            map[CONFIG] = Option(CONFIG, "removable.list")
            map[SAFE_MODE] = Option(SAFE_MODE, false)
            map[HELP] = Option(HELP, false)
            map
        }
    }

    fun default(): Option<*> =
        defaults[this] ?: throw RuntimeException("Option type $this did not have a default value!")
}

data class Option<T>(val optionType: OptionType, val value: T)