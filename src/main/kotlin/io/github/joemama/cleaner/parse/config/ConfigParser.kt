package io.github.joemama.cleaner.parse.config

import io.github.joemama.cleaner.assert
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class ConfigParser(private val path: String) {
    private val file: File = File(this.path)
    private val tokens: Set<String>

    init {
        assert(file.exists()) { "Config file could not be loaded from `${file.path}`!" }

        val reader = BufferedReader(FileReader(this.file))
        this.tokens = this.parseTokens(reader)

        reader.close()
    }

    private fun parseTokens(reader: BufferedReader): Set<String> = reader.lineSequence()
        .map { it.trim() }
        .filter { !it.startsWith('\n') }
        .filter { !it.startsWith('#') }
        .filter { it.isNotEmpty() }
        .toSet()

    fun validateName(fileName: String, isDirectory: Boolean): Boolean {
        this.tokens
            .filterNot { it.contains('*') }
            .forEach {
                when (isDirectory) {
                    true -> if (this.isDirectoryValid(fileName, it)) return true
                    else -> if (this.isFileValid(fileName, it)) return true
                }
            }

        return this.tokens
            .filter { it.contains('*') }
            .map { it.split("*") }
            .any { splitToken ->
                when (isDirectory) {
                    true -> this.validateWildcardDir(fileName, splitToken)
                    else -> this.validateWildcardFile(fileName, splitToken)
                }
            }
    }

    private fun validateWildcardFile(fileName: String, splitToken: List<String>): Boolean =
        splitToken.all { fileName.contains(it) }

    private fun validateWildcardDir(fileName: String, splitToken: List<String>): Boolean {
        val correctedFileName = "$fileName/"
        return splitToken.all { correctedFileName.contains(it) }
    }

    private fun isFileValid(fileName: String, token: String): Boolean =
        fileName.startsWith(token)

    private fun isDirectoryValid(fileName: String, token: String): Boolean =
        "$fileName/" == token
}