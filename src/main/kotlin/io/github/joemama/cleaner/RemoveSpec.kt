package io.github.joemama.cleaner

import io.github.joemama.cleaner.parse.config.ConfigParser
import io.github.joemama.cleaner.parse.option.OptionParser
import io.github.joemama.cleaner.parse.option.OptionType
import java.io.File

data class RemoveSpec(val optionParser: OptionParser) {
    companion object {
        @JvmStatic
        val ACCEPT_REPLIES = setOf("Yes", "Y", "y", "\n", "")
    }

    private val configParser: ConfigParser =
        ConfigParser(this.optionParser[OptionType.CONFIG].value.toString())

    fun searchAndAttemptRemoval() {
        val dirPath = this.optionParser[OptionType.TARGET_DIR].value.toString()
        val recursive = this.optionParser[OptionType.RECURSIVE].value.toString().toBooleanStrict()

        val suggestedFiles: MutableList<File> = mutableListOf()
        logTime({
            File(dirPath).listFiles()?.forEach { file ->
                this.search(file, recursive) { suggestedFiles.add(it) }
            }
        }) { "Locating files took " }

        this.remove(suggestedFiles)
    }

    private fun remove(files: List<File>) {
        if (this.wantsDeletion(files)) {
            logTime({
                this.deleteAll(files)
            }) { "Deletion took " }
        }
    }

    private fun deleteAll(files: List<File>) {
        val inSafeMode = this.optionParser[OptionType.SAFE_MODE].value.toString().toBooleanStrict()
        if (inSafeMode) {
            println()
            println("YOU ARE CURRENTLY IN SAFE-MODE SO NO FILES WILL BE DELETED.")
        }
        println()

        files.forEach {
            if (!inSafeMode) {
                it.deleteRecursively()
            }
            println("Deleting ${it.absolutePath}")
        }
    }

    private fun wantsDeletion(files: List<File>): Boolean {
        var input: String
        do {
            println("WARNING: You are about to remove ${files.size} files/folders!!!")
            print("Are you sure you want to continue? [Y/n/l (list all files then ask again)] ")
            input = readLine() ?: ""

            if (input == "l") {
                this.listFiles(files)
            }
        } while (input.isNotEmpty() && input == "l")

        return input in ACCEPT_REPLIES
    }

    private fun listFiles(files: Iterable<File>) {
        files.forEach { println(it.absolutePath) }
    }

    private fun search(file: File, isRecursive: Boolean = false, consumer: (File) -> Unit = { }) {
        // TODO: Figure out whether to allow git or not
        if (file.name.contains(".git"))
            return
        if (file.isDirectory && isRecursive) {
            file.listFiles()?.forEach { this.search(
                file = it,
                isRecursive = true /* It's obvious since we are &&ing it */,
                consumer)
            }
        }

        if (this.configParser.validateName(file.name, file.isDirectory)) {
            consumer(file)
            return
        }
    }
}
