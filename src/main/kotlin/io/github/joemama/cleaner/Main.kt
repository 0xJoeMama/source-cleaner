package io.github.joemama.cleaner

import io.github.joemama.cleaner.parse.option.OptionParser
import io.github.joemama.cleaner.parse.option.OptionType

fun main(args: Array<String>) {
    val optionParser = OptionParser(args)

    if (!optionParser[OptionType.HELP].value.toString().toBooleanStrict()) {
        val spec = RemoveSpec(optionParser)
        spec.searchAndAttemptRemoval()
    } else {
        promptUsage()
    }
}

fun promptUsage() {
    val copyrightSymbol = "\u00a9"
    val usage = """
        Source Cleaner by 0xJoeMama$copyrightSymbol
        
        Delete everything you want! (Use at your own risk):
        
        -d <directory-path>     Specify working directory(default is `./`).
        -c <config-path>        Specify config(default is the one bundled with the app).
        -r                      Search recursively from current directory. This includes all subdirectories.
        -s                      Run in safe mode. This prevents any files from being deleted.
        -h                      Show this prompt.
        
        This program requires Java 11 to run. Use with different versions at your own risk.
        (Honestly newer versions should work but I don't promise anything...)
    """.trimIndent()

    println(usage)
}

fun logTime(block: () -> Unit, msg: () -> String = { "Task took " }) {
    val begin = System.currentTimeMillis()
    block()
    println("${msg()}${System.currentTimeMillis() - begin}ms")
}

fun assert(condition: Boolean, msg: () -> String): Unit =
    if (!condition) throw IllegalStateException(msg()) else Unit