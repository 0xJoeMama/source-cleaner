# Jetbrains Source Cleaner

Remove any files you wish with the use of just one command.

## Usage

This program was written in order to assist in cleaning up my workspaces from cache, built artifacts, logs, etc.\
However there is nothing stopping someone from using it to clean any kind of folder.

## Get Started

You first need to have the JRE installed.\
Java 11 is the only tested version but any version after it should work as well.

After you made sure you are running Java 11 or later,\
you can start by downloading a binary from this website.
***TODO: Add release site***
---

### Config

Now you need to create a config file. Make sure it's a UTF-8\
formatted file.

```
# You can write comments in it using a `#` before a line
# Directories are noted using / at the end of the name like this
.idea/
# You can define files by just removing the /
Project.iml

# You can also define wildcards using an `*`
.*/
# This will remove all directories whose name starts with a . 
# aka all hidden directories

You could also do something like that for files
*.jpg
# This will remove all jpg files.
```

---

### Command Line Arguments

There are 3 main arguments most people would care about:

| Argument | Usage                                                    | Default Value      |
|----------|----------------------------------------------------------|--------------------|
| -r       | Makes the search recursive.                              | `false`            |
| -d       | Allows you to specify the working directory.             | `./`               |
| -c       | Allows you to specify the path to a config file.         | `./removable.list` |
| -s       | Runs the program in safe-mode. No files will be deleted. | `false`            |
| -h       | Show the help dialog.                                    | `false`            |

Example command : `java -jar source-cleaner.jar -r -d ./ -c -s ./removable.list`

## WIP

This project can have changes made to it and/or may cause issues.\
***!! Use at your own risk !!***